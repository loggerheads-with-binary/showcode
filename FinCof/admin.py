from django.contrib import admin
from . import models
from import_export.admin import ImportExportModelAdmin

# Register your models here.


class CompanyAdmin(ImportExportModelAdmin):
    list_display = ('name', 'email', 'username', 'password')


admin.site.register(models.Company, CompanyAdmin)


class OfferingAdmin(ImportExportModelAdmin):
    list_display = ('id', 'company', 'sector', 'applications', 'interns',
                    'duration', 'stipend', 'verified')
    list_filter = ['track']


admin.site.register(models.Offering, OfferingAdmin)


class StudentAdmin(ImportExportModelAdmin):
    list_display = ('name', 'roll', 'email', 'interns', 'year')
    list_filter = ['year']

admin.site.register(models.Student, StudentAdmin)


@admin.register(models.Application)
class ApplicationAdmin(ImportExportModelAdmin):
    list_display = ['student', 'id', 'offering']


@admin.register(models.Cart)
class CartAdmin(ImportExportModelAdmin):
    list_display = ['student', 'offering']


@admin.register(models.Activeurl)
class ActiveurlAdmin(admin.ModelAdmin):
    list_display = ['name', 'publish']
