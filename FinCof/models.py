from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver
import random
# Create your models here.


def upload_path(instance, filename):
    ext = filename.split('.')[-1]
    filename = "%s.%s" % (instance.name, ext)
    return '/'.join(['fincof', filename])


class Company(models.Model):
    name = models.CharField(max_length=255, null=True, blank=True)
    email = models.EmailField(max_length=255, null=True, blank=True)
    logo = models.FileField(blank=True, null=True, upload_to=upload_path)
    contact = models.BigIntegerField(null=True, blank=True)
    website = models.URLField(max_length=255, null=True, blank=True)
    username = models.CharField(default='shivansh', max_length=50)
    password = models.CharField(default='shivansh', max_length=50)

    def __str__(self):
        return f"{self.name}"


@receiver(post_save, sender=Company)
def comp(sender, instance, created, **kwargs):
    if created:
        instance.password = User.objects.make_random_password()
        instance.username = instance.name.lower()
        instance.save()


TRACK = (
    ('', 'Choose'),
    ('cofounder', 'cofounder'),
    ('intern', 'intern'),
    ('freelancer', 'freelancer'),
)

SECTOR = (
    ('', 'Choose'),
    ('dev', 'Software Development, Web and App Development'),
    ('content', 'Content Creation, Social Media and Marketing'),
    ('business', 'Business, Consulting or Finance'),
    ('eng', 'Engineering, Industry and Research'),
    ('sales', 'Sales and Product Management'),
    ('ai', 'Deep Learning and Artificial Intelligence'),
    ('data', 'Data Science and Data Analyst'),
    ('design', 'Design')
)


class Offering(models.Model):
    company = models.ForeignKey(
        Company, on_delete=models.CASCADE, related_name='company')
    track = models.CharField(max_length=20, null=True,
                             blank=True, choices=TRACK)
    
    sector = models.CharField(
        max_length=255, null=True, blank=True, choices=SECTOR)
    interns = models.IntegerField(default=1, verbose_name='no_interns')
    keywords = models.TextField(null = True)
    pre_requisite = models.TextField(null=True, blank=True,)
    job = models.TextField(null=True, blank=True,
                           verbose_name='job_discription')
    title = models.TextField(null = True , blank = True , verbose_name = 'Job Title')
    duration = models.CharField(max_length=50, default='2 months')
    stipend = models.CharField(null=True, max_length=100)
    verified = models.BooleanField(default=True)
    selections = models.IntegerField(default=10)
    applications = models.IntegerField(default=0)
    

    def __str__(self):
        return f"{self.company.name} -- {self.sector}"


class Student(models.Model):
    name = models.CharField(max_length=255, null=True, blank=True)
    photo = models.FileField(blank=True, null=True, upload_to=upload_path)
    year = models.CharField(blank=True, null=True, max_length=10)
    department = models.CharField(blank=True, null=True, max_length=50)
    roll = models.CharField(max_length=9, null=True, blank=True)
    email = models.EmailField(
        max_length=255, null=True, blank=True, unique=True)
    contact = models.BigIntegerField(null=True, blank=True)
    info = models.BooleanField(default=False)
    error = models.CharField(max_length=255, null=True, blank=True)
    interns = models.IntegerField(default=0)

    def __str__(self):
        return f"{self.roll} -- {self.name}"


def path(instance, filename):
    ext = filename.split('.')[-1]
    filename = "%s_%s.%s" % (instance.student.name,
                             random.randint(1, 100), ext)
    return '/'.join(['fincof', filename])


STATUS = (
    ('pending', 'pending'),
    ('selected', 'selected'),
    ('rejected', 'rejected'),
)


class Application(models.Model):
    student = models.ForeignKey(
        Student, on_delete=models.CASCADE, related_name='stud')
    offering = models.ForeignKey(
        Offering, on_delete=models.CASCADE, related_name='profile')
    resume = models.FileField()
    status = models.CharField(max_length=20, choices=STATUS, default='pending')
    date = models.CharField(max_length=20, null=True, blank=True)
    time = models.CharField(max_length=20, null=True, blank=True)
    meet = models.CharField(max_length=251, null=True, blank=True)

    class Meta:
        unique_together = ('student', 'offering')


@receiver(post_save, sender=Application)
def appli(sender, instance, created, **kwargs):
    if instance.offering.applications < 10000:
        instance.offering.applications = Application.objects.filter(
            offering=instance.offering).count()
        instance.offering.save()

        if instance.student.interns <= 500:
            instance.student.interns = Application.objects.filter(
                student=instance.student).count()
            instance.student.save()
        else:
            instance.student.error = 'You have applied for maximum allowed offerings'
            instance.save()

    else:
        instance.student.error = 'Maximum number of Application Reached'
        instance.save()


class Activeurl(models.Model):
    name = models.CharField(max_length=20, null=True, blank=True)
    publish = models.BooleanField(default=True)


class Cart(models.Model):
    student = models.ForeignKey(
        Student, on_delete=models.CASCADE, related_name='cartstud')
    offering = models.ForeignKey(
        Offering, on_delete=models.CASCADE, related_name='cart')

    class Meta:
        unique_together = ('student', 'offering')
