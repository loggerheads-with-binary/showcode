from rest_framework import serializers
from . import models
from .models import Offering, Cart
from django.contrib.auth.models import User


class CompanySerilizer(serializers.ModelSerializer):

    class Meta:
        model = models.Company
        fields = ['id', 'name', 'email', 'contact', 'website']


class OfferingSerilizer(serializers.ModelSerializer):
    company = CompanySerilizer(read_only=True)
    # company = serializers.SlugRelatedField(read_only=True, slug_field='name')

    class Meta:
        model = Offering
        fields = ('id', 'track', 'sector', 'interns', 'pre_requisite',
                  'job', 'duration', 'stipend', 'company')


class StudentSerilizer(serializers.ModelSerializer):

    class Meta:
        model = models.Student
        fields = '__all__'


class ApplicationSerilizer(serializers.ModelSerializer):
    offering = OfferingSerilizer(read_only=True)
    student = StudentSerilizer(read_only=True)

    class Meta:
        model = models.Application
        fields = ('id', "resume", "student", 'status',
                  'offering', 'date', 'time', 'meet')


class ActiveurlSerilizer(serializers.ModelSerializer):

    class Meta:
        model = models.Activeurl
        fields = '__all__'


class CartSerilizer(serializers.ModelSerializer):
    # student = StudentSerilizer(read_only=True)
    offering = OfferingSerilizer(read_only=True)

    class Meta:
        model = Cart
        fields = ('id',  'offering')
        # fields = '__all__'


class AppSerilizer(serializers.ModelSerializer):
    # offering = OfferingSerilizer(read_only=True)
    student = StudentSerilizer(read_only=True)

    class Meta:
        model = models.Application
        fields = ("student",)
