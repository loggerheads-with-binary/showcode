from django.contrib import admin
from django.urls import include, path
from . import views


urlpatterns = [
    path('complog/', views.comp_login),
    path('stud/', views.student),
    path('test/', views.test),
    path('applications/', views.applications),
    path('comp_details/', views.comp_details),
    path('cofounder/', views.cofounder),
    path('intern/', views.intern),
    path('detail/', views.detail),
    path('cart/', views.cart),
    path('add_cart/', views.add_cart),
    path('update/', views.update),
    path('all/', views.all_applications),
    path('company_app/', views.comp_applications),
    path('shortlist/', views.shortlist_applications),
    path('final/', views.final_applications),
    path('check/', views.check),
    path('verify/', views.verify),
    path('com_action/', views.com_action),
    path('selected_applications/', views.selected_applications),
    path('sector/', views.sector),
    path('tocsv/', views.tocsv),
    path('interview/', views.interview),
    path('drop/', views.drop),
]
