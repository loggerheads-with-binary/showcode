from rest_framework import status
from rest_framework.parsers import FileUploadParser
from django.shortcuts import render
from rest_framework import viewsets
from . import serializers, models
from .serializers import OfferingSerilizer
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.decorators import api_view
from django.contrib.auth.models import User
from rest_framework.authtoken.views import ObtainAuthToken
from django.http import HttpResponse
from django.db.models import ObjectDoesNotExist
from django.http import FileResponse
from django.core.mail import send_mail
import json

# Create your views here.


class CompanyViewSet(viewsets.ModelViewSet):
    queryset = models.Company.objects.all()
    serializer_class = serializers.CompanySerilizer


class OfferingViewSet(viewsets.ModelViewSet):
    queryset = models.Offering.objects.all()
    serializer_class = serializers.OfferingSerilizer


class StudentViewSet(viewsets.ModelViewSet):
    queryset = models.Student.objects.all()
    serializer_class = serializers.StudentSerilizer


class ApplicationViewSet(viewsets.ModelViewSet):
    queryset = models.Application.objects.all()
    serializer_class = serializers.ApplicationSerilizer


@api_view(['POST'])
def comp_login(request):
    reqData = request.data
    try:
        comp = models.Company.objects.get(username=reqData['username'])
        if comp.password == reqData['password']:
            comp_serializer = serializers.CompanySerilizer(comp)
            # return Response(comp_serializer.data)
            return Response({
                'status': 'success',
                'id': comp_serializer.data['id'],
            })
        else:
            return Response({'status': 'incorrect password'})

    except ObjectDoesNotExist:
        return Response({'status': 'Either username is incorrect or company not registered'})


@api_view(['POST'])
def comp_details(request):
    reqData = request.data
    temp = models.Company.objects.get(
        pk=reqData['id'], username=reqData['username'])
    temp1 = models.Offering.objects.filter(company=reqData['id'])
    comp_serializer = serializers.CompanySerilizer(temp)
    offering_serializer = serializers.OfferingSerilizer(temp1, many=True)
    return Response({
        'company': comp_serializer.data,
        'offering': offering_serializer.data,
    })


@api_view(['GET'])
def cofounder(request):
    user = request.user
    stud = models.Student.objects.get(email=user.email)
    offerings = models.Offering.objects.filter(
        track="cofounder", verified=True).order_by('-selections')
    offering_serializer = serializers.OfferingSerilizer(offerings, many=True)
    return Response(offering_serializer.data)


@api_view(['POST'])
def intern(request):
    user = request.user
    stud = models.Student.objects.get(email=user.email)
    reqData = request.data
    offerings = models.Offering.objects.exclude(
        track="cofounder").filter(sector=reqData['sector'], verified=True).order_by('-selections')
    offering_serializer = OfferingSerilizer(offerings, many=True)
    return Response(offering_serializer.data)


@api_view(['POST'])
def student(request):
    reqData = request.data
    stud, created = models.Student.objects.get_or_create(
        email=reqData['email'])
    student_serializer = serializers.StudentSerilizer(stud)
    if created:
        stud.name = reqData['name']
        stud.roll = reqData['roll']
        stud.contact = reqData['contact']
        stud.year = reqData['year']
        stud.department = reqData['dept']
        stud.info = True
        stud.save()
        return Response({'status': 'created'})
    else:
        stud.name = reqData['name']
        stud.roll = reqData['roll']
        stud.contact = reqData['contact']
        stud.year = reqData['year']
        stud.department = reqData['dept']
        stud.save()
        # return Response(student_serializer.data)
        return Response({'status': 'present'})


@api_view(['PUT'])
def update(request):
    user = request.user
    reqData = request.data
    stud = models.Student.objects.get(email=user.email)
    student_serializer = serializers.StudentSerilizer(stud)
    stud.roll = reqData['roll']
    stud.year = reqData['year']
    stud.department = reqData['dept']
    stud.contact = reqData['contact']
    stud.save()
    return Response({'status': 'updated'})
    # return Response(student_serializer.data)


@api_view(['GET'])
def detail(request):
    user = request.user
    # reqData = request.data
    stud = models.Student.objects.get(email=user.email)
    student_serializer = serializers.StudentSerilizer(stud)
    return Response(student_serializer.data)


@api_view(['GET'])
def test(request):
    temp = models.Offering.objects.all()
    ser = serializers.OfferingSerilizer(temp, many=True)
    return Response(ser.data)


@api_view(['POST'])
def add_cart(request):
    user = request.user
    reqData = request.data
    stud = models.Student.objects.get(email=user.email)
    off = models.Offering.objects.get(pk=reqData['off'])
    try:
        cart = models.Cart.objects.create(student=stud, offering=off)
        cart.save()
        cart_serializer = serializers.CartSerilizer(cart)
        return Response({'status': 'Success'})
    except:
        return Response({'status': 'Already in Cart'})


@api_view(['GET'])
def cart(request):
    user = request.user
    stud = models.Student.objects.get(email=user.email)
    try:
        cart = models.Cart.objects.filter(student=stud)
        cart_serializer = serializers.CartSerilizer(cart, many=True)
        return Response(cart_serializer.data)
    except ObjectDoesNotExist:
        return Response({'Contact Technical Team'})


@api_view(['POST'])
def applications(request):
    reqData = request.data
    user = request.user
    action = reqData['action']
    if action == 'add':
        stud = models.Student.objects.get(email=user.email)
        if stud.interns < 500:
            off = models.Offering.objects.get(pk=reqData['off'])
            appli = models.Application.objects.create(
                student=stud, offering=off)
            appli.resume = request.FILES['resume']
            appli.save()
            appli_serializer = serializers.ApplicationSerilizer(appli)
            cart = models.Cart.objects.filter(student=stud, offering=off)
            cart_serializer = serializers.CartSerilizer(cart, many=True)
            cart.delete()
            return Response({'status': 'Success'})
        else:
            return Response({'status': 'max applications reached'})
    elif action == 'del':
        reqData = request.data
        user = request.user
        stud = models.Student.objects.get(email=user.email)
        off = models.Offering.objects.get(pk=reqData['off'])
        cart = models.Cart.objects.filter(student=stud, offering=off)
        cart_serializer = serializers.CartSerilizer(cart, many=True)
        cart.delete()
        return Response({'status': 'deleted'})


@api_view(['GET'])
def all_applications(request):
    user = request.user
    stud = models.Student.objects.get(email=user.email)
    appli = models.Application.objects.filter(student=stud)
    appli_serializer = serializers.ApplicationSerilizer(appli, many=True)
    return Response(appli_serializer.data)


@api_view(['POST'])
def comp_applications(request):
    reqData = request.data
    off = models.Offering.objects.get(pk=reqData['off'])
    appli = models.Application.objects.filter(offering=off, status='pending')
    appli_serializer = serializers.ApplicationSerilizer(appli, many=True)
    return Response(appli_serializer.data)


@api_view(['POST'])
def selected_applications(request):
    reqData = request.data
    off = models.Offering.objects.get(pk=reqData['off'])
    appli = models.Application.objects.filter(offering=off, status='selected')
    appli_serializer = serializers.ApplicationSerilizer(appli, many=True)
    return Response(appli_serializer.data)


@api_view(['POST'])
def final_applications(request):
    reqData = request.data
    off = models.Offering.objects.get(pk=reqData['off'])
    appli = models.Application.objects.filter(offering=off, status='selected')
    appli_serializer = serializers.ApplicationSerilizer(appli, many=True)
    return Response(appli_serializer.data)


@api_view(['POST'])
def shortlist_applications(request):
    reqData = request.data
    appli = models.Application.objects.get(pk=reqData['id'])
    appli_serializer = serializers.ApplicationSerilizer(appli)
    action = reqData['action']
    if action == 'select':
        appli.status = 'selected'
        appli.save()
        return Response(appli_serializer.data)
    elif action == 'reject':
        appli.status = 'rejected'
        appli.save()
        return Response(appli_serializer.data)
    else:
        return Response(appli_serializer.data)


@api_view(['GET'])
def check(request):
    user = request.user
    stud = models.Student.objects.get(email=user.email)
    stud_serializer = serializers.StudentSerilizer(stud)
    return Response({'detail': 'user'})


@api_view(['GET'])
def verify(request):
    verify = models.Activeurl.objects.get(name="student")
    ser = serializers.ActiveurlSerilizer(verify)
    return Response(ser.data)


@api_view(['POST'])
def com_action(request):
    reqData = request.data
    appli = models.Application.objects.get(pk=reqData['appli'])
    appli_serializer = serializers.ApplicationSerilizer(appli)
    action = reqData['action']
    if action == "selected":
        appli.status = "selected"
        appli.save()
        send_mail_fincof(appli.offering.company.name,
                         appli.student.email, appli.offering.sector)
    elif action == "rejected":
        appli.status = "rejected"
        appli.save()

    return Response({'status': 'success'})


@api_view(['POST'])
def sector(request):
    reqData = request.data
    sec = models.Offering.objects.get(pk=reqData['sector'])
    ser = serializers.OfferingSerilizer(sec)
    return Response(ser.data)


def send_mail_fincof(comp, email, sector):
    ('dev', 'Software Development, Web and App Development'),
    ('content', 'Content Creation, Social Media and Marketing'),
    ('business', 'Business, Consulting or Finance'),
    ('eng', 'Engineering, Industry and Research'),
    ('sales', 'Sales and Product Management'),
    ('ai', 'Deep Learning and Artificial Intelligence'),
    ('data', 'Data Science and Data Analyst'),
    ('design', 'Design')

    if sector == "dev":
        sector = "Software Development, Web and App Development"
    elif sector == "content":
        sector = "Content Creation, Social Media and Marketing"
    elif sector == "business":
        sector = "Business, Consulting or Finance"
    elif sector == "eng":
        sector = "Engineering, Industry and Research"
    elif sector == "sales":
        sector = "Sales and Product Management"
    elif sector == "data":
        sector = "Data Science and Data Analyst"
    elif sector == "ai":
        sector = "Deep Learning and Artificial Intelligence"

    sub = "FInCoF | Congratulations on being shortlisted for: " + comp
    body = f"""
        <html>
        <body>
        <p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">Hello,</span></p><br>
<p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">Greetings from The Entrepreneurship Cell, IIT Bombay.</span></p>
<p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">Congratulations! We are pleased to inform you that you&rsquo;ve been shortlisted for the interview round of {comp} in {sector} profile.</span></p>
<p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">Interview rounds will start from 14th June, 2021. If you want to check the status of your application in other companies, kindly visit <a href="https://www.ecell.in/fincof/#/student">https://www.ecell.in/fincof/#/student</a></span></p>
<p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">All the best for your interviews.&nbsp;</span></p>
<p><br></p>
<p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">Please feel free to reach out to me if you have any queries.</span></p>
<p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><br></p>
<p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">Thanks &amp; Regards,</span></p>
<p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">Suryansh Bhandari</span></p>
<p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">Events &amp; PR Head</span></p>
<p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">E-Cell IIT Bombay</span></p>
<p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><span style="font-size:11pt;font-family:Arial;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">+91-8896244444</span></p>
        </body>
        </html>
        """
    send_mail(sub, body, 'fincof@ecell.in',
              [email, 'shivansh@ecell.in'], html_message=body)


@api_view(['GET'])
def tocsv(request):
    reqData = request.data
    off = models.Offering.objects.get(pk=2903)
    appli = models.Application.objects.filter(offering=off, status='selected')
    appli_serializer = serializers.AppSerilizer(appli, many=True)
    return Response(appli_serializer.data)


@api_view(['POST'])
def interview(request):
    reqData = request.data
    appli = models.Application.objects.get(pk=reqData['id'])
    appli_serializer = serializers.ApplicationSerilizer(appli)
    act = reqData['act']
    if act == 'date':
        appli.date = reqData['date']
    elif act == 'time':
        appli.time = reqData['time']
    elif act == 'meet':
        appli.meet = reqData['meet']
    else:
        pass

    if (appli.date is not None) and ((appli.time is not None) and (appli.meet is not None)):
        interviewemails(appli.student.email, appli.offering.company.name, appli.date, appli.time, appli.meet,
                        appli.offering.track, appli.offering.duration, appli.offering.stipend, appli.offering.job, appli.offering.company.email)
    appli.save()

    return Response(appli_serializer.data)


@api_view(['POST'])
def drop(request):
    reqData = request.data
    appli = models.Application.objects.get(pk=reqData['id'])
    appli_serializer = serializers.ApplicationSerilizer(appli)
    appli.status = "rejected"
    appli.save()
    dropemail(appli.offering.company.email, appli.student.name)
    return Response(appli_serializer.data)


def dropemail(email, name):

    sub = "Interview Schedule Update"
    body = f"""
        <html>
        <body>
        <p><span style="color: rgb(34, 34, 34); font-family: Arial, Helvetica, sans-serif; font-size: 15px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial; float: none; display: inline !important;">Dear Sir/ Madam,</span></p>
<p><br></p>
<div style="color: rgb(34, 34, 34); font-family: Arial, Helvetica, sans-serif; font-size: small; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;"><span style="font-size: 15px;">One of your shortlisted candidates, {name}, has opted out of the selection process and does not wish to proceed with the interview. Their interview slot has been removed from your company login portal.</span></div>
<p><br></p>
<div style="color: rgb(34, 34, 34); font-family: Arial, Helvetica, sans-serif; font-size: small; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;"><span style="font-size: 15px;">Thanks and Regards</span></div>
<p><br></p>
<p>Vishesh Agarwal</p>
<p>Events &amp; PR Head, E Cell</p>
<p>IIT Bombay</p>
<p>+91-9717369148</p></body>
        </html>
        """
    send_mail(sub, body, 'fincof@ecell.in',
              [email, 'shivansh@ecell.in'], html_message=body)

    return


def interviewemails(email, company, date, time, link, track, duration, stipend, job, compemail):

    sub = "Interview Details"
    body = f"""
        <html>
        <body>
        <p>Hello,</p>
<p>Greetings from E Cell, IIT Bombay</p>
<p>You have been shortlisted for the interview for {company}. You interview is scheduled at</p>
<p><br>Date: {date}</p>
<p>Time: {time}</p>
<p>Meeting Link: {link}</p>
<p>Kindly be present five minutes prior to the interview.</p>
<p>If you don&apos;t wish to interview for the company, you can opt out of the selection&nbsp;process by selecting &apos;Drop&apos; on the student portal (<a data-saferedirecturl="https://www.google.com/url?q=http://ecell.in/fincof&source=gmail&ust=1623833892574000&usg=AFQjCNGAmkFhjT_2Gsvn5RZpiSXL1uD_nw" href="http://ecell.in/fincof" style="color: rgb(17, 85, 204);" target="_blank">ecell.in/fincof</a>)</p>
<p>If due to any major issues you cannot appear at the interview time, you can try to ask for a rescheduling at {compemail} and cc <a href="mailto:fincof@ecell.in" style="color: rgb(17, 85, 204);" target="_blank">fincof@ecell.in</a>, providing proper reasoning for the same.</p>
<p><br></p>
<p>The details of the profile are as follows</p>
<p>{company} -- {track}</p>
<p>Job Description: {job}</p>
<p>Duration: {duration}</p>
<p>Stipend: {stipend}</p>
<p><br></p>
<p><span style="color: rgb(255, 0, 0);">NOTE- You may get multiple emails from the company about the schedule, because company may be rescheduling your interview. Kindly check the latest time on the portal.</span></p>
<p><br><br>Thanks &amp; Regards</p>
<p><br></p>
<p>Vishesh Agarwal</p>
<p>Events &amp; PR Head</p>
<p>E Cell IIT Bombay</p>
<p>9717369148</p>
<p><br style="color: rgb(136, 136, 136); font-family: Arial, Helvetica, sans-serif; font-size: small; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;"></p></body>
        </html>
        """
    send_mail(sub, body, 'fincof@ecell.in',
              [email, 'shivansh@ecell.in'], html_message=body)

    return
