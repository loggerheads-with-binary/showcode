from django.contrib import admin
from django.contrib.auth.models import User
from .models import VSMProfile, FAQ, Instruction
# from organizations.models import Organization, OrganizationUser, OrganizationOwner
from import_export.admin import ImportExportModelAdmin
# from django.contrib.auth import get_user_model
# import csv, datetime
from django.contrib import admin
from . import models
from import_export import resources


class VSMProfileAdmin(ImportExportModelAdmin):
    list_display = ('user', 'net_worth', 'cash', 'is_iitb', 'roll_number')
    list_filter = ()
    search_fields = ['user__username', 'user__first_name', 'user__email',
                     'user__last_name', 'roll_number', 'cash', 'is_iitb', ]


# Register your models here.
admin.site.register(VSMProfile, VSMProfileAdmin)
admin.site.register(FAQ)
admin.site.register(Instruction)
class NewsAdmin(ImportExportModelAdmin):
    list_display = ('title', 'content',)
admin.site.register(models.News, NewsAdmin)


# class UserResource(resources.ModelResource):

#     class Meta:
#         model = User

# class BookAdmin(ImportExportModelAdmin):
#     resource_class = UserResource

# admin.site.register(User, BookAdmin)


class CompanyAdmin(admin.ModelAdmin):
    list_display = ('name', 'code', 'current_market_price',
                    'change', 'stocks_availible')
    list_filter = ()
    search_fields = ['name', 'code', ]


admin.site.register(models.Company, CompanyAdmin)


class TransactionAdmin(admin.ModelAdmin):
    list_display = ('user', 'company', 'quantity',
                    'transaction_type', 'bid_price', 'verified', 'extra')
    list_filter = ('verified', )
    readonly_fields = ['verified', ]
    search_fields = ['company', 'user', 'quantity', ]


admin.site.register(models.Transaction, TransactionAdmin)


class OrderAdmin(admin.ModelAdmin):
    list_display = ('user', 'company', 'quantity',
                    'transaction_type', 'bid_price', 'executed', 'extra')
    list_filter = ('executed', )
    readonly_fields = ['executed', ]
    search_fields = ['company', 'user', 'quantity', ]


admin.site.register(models.Order, OrderAdmin)


class HoldingAdmin(admin.ModelAdmin):
    list_display = ('company', 'user', 'quantity',)
    list_filter = ()
    search_fields = ['company', 'user', ]


admin.site.register(models.Holding, HoldingAdmin)


class CmpAdmin(admin.ModelAdmin):
    list_display = ('company', 'cmp', 'change', 'timestamp')
    list_filter = ('company', 'timestamp')
    search_fields = ['company', 'cmp', 'timestamp', ]


admin.site.register(models.CompanyCMPRecord, CmpAdmin)

admin.site.register(models.Timepass)
# admin.site.unregister(Organization)
# admin.site.unregister(OrganizationUser)
# admin.site.unregister(OrganizationOwner)

# class RegistrationAdmin(ImportExportModelAdmin):
#     list_display = ('name', 'email', 'whatsapp', 'refferal', 'uname')

# admin.site.register(models.Registration, RegistrationAdmin)

# User = get_user_model()
