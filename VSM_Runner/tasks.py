from decimal import Decimal
from celery import Celery, shared_task
from rest_framework.decorators import api_view
from .models import CompanyCMPRecord, Company, Holding, Transaction, VSMProfile,  Order
from .serializers import CompanySerializer, CampanyJson, VSMProfileSerializer
from django.contrib.auth.models import User, Group
from decimal import Decimal
import random
from django.conf import settings
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from django.core.cache import cache
import requests
import boto3
from django.core.serializers import serialize
import queue
import time
from threading import Thread
import json


app = Celery('tasks', broker='redis://127.0.0.1:6379',
             backend='redis://127.0.0.1:6379')
app.conf.enable_utc = False
app.conf.update(timezone='Asia/Kolkata')
app.conf.timezone = 'Asia/Kolkata'


# @app.task
# def alpha():
#     for i in range(5):
#         print('hey1')
#     return "Done"


@app.task
def make_transaction(quantity, transac_type, code, email):
    quant = int(quantity)
    transac_type = transac_type
    code = code
    company = Company.objects.get(code=code)
    user = User.objects.get(email=email)
    vsm_profile = VSMProfile.objects.get(user=user)
    price = company.current_market_price
    transac = Transaction.objects.create(
        user=vsm_profile, company=company, transaction_type=transac_type, quantity=quant, bid_price=price)
    transac.save()
    return 'Transaction created'


@app.task
def order(quantity, transac_type, code, email):
    quant = int(quantity)
    transac_type = transac_type
    code = code
    company = Company.objects.get(code=code)
    user = User.objects.get(email=email)
    vsm_profile = VSMProfile.objects.get(user=user)
    price = company.current_market_price
    order = Order.objects.create(
        user=vsm_profile, company=company, transaction_type=transac_type, quantity=quant, bid_price=price)
    order.save()
    return 'Order created'


@app.task
def upt():
    stockpicker = [
        'ASIANPAINT',
        'EICHERMOT',
        'M&M',
        'HCLTECH',
        'BHARTIARTL',
        'HDFCBANK',
        'MARUTI',
        'GRASIM',
        'INFY',
        'ICICIBANK',
        'HDFC',
        'WIPRO',
        'BAJAJ-AUTO',
        'CIPLA',
        'TCS',
        'BAJAJFINSV',
        'BRITANNIA',
        'KOTAKBANK',
        'NESTLEIND',
        'DRREDDY',
        'TECHM',
        'TITAN',
        'RELIANCE',
        'COALINDIA',
        'HEROMOTOCO',
        'INDUSINDBK',
        'IOC',
        'UPL',
        'HINDALCO',
        'POWERGRID',
        'TATAMOTORS',
        'HINDUNILVR',
        'ULTRACEMCO',
        'ADANIPORTS',
        'BAJFINANCE',
        'ONGC',
        'NTPC',
        'ITC',
        'AXISBANK',
        'SHREECEM',
        'DIVISLAB',
        'SBIN',
        'JSWSTEEL',
        'TATASTEEL',
    ]

    n_threads = len(stockpicker)

    thread_list = []
    que = queue.Queue()
    start = time.time()
    companies = []
    from_cache = []
    for i in range(n_threads):

        code = stockpicker[i]
        queryset = Company.objects.get(code=code)
        serializer_class = CampanyJson(queryset)

        thread = Thread(target=lambda q, arg1: q.put(
            {'company': serializer_class.data}), args=(que, stockpicker[i]))
        thread_list.append(thread)
        thread_list[i].start()

    for thread in thread_list:
        thread.join()

    while not que.empty():
        result = que.get()
        companies.append(result)

    end = time.time()
    time_taken = end - start
    print(time_taken)

    with open("/home/backend/company_market.json", "w") as out:
        # data = serialize("json", companies)
        out.write(json.dumps(companies))

    AWS_ACCESS_KEY_ID = 'AKIAXVIULOAALQE5Y77Q'
    WS_SECRET_ACCESS_KEY = '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'

    # s3 = boto3.resource('s3',
    #      aws_access_key_id=ACCESS_ID,
    #      aws_secret_access_key= ACCESS_KEY)

    s3 = boto3.client('s3',
                      aws_access_key_id=AWS_ACCESS_KEY_ID,
                      aws_secret_access_key=WS_SECRET_ACCESS_KEY)
    with open("/home/backend/company_market.json", "rb") as f:
        s3.upload_fileobj(f, "2k21", "vsm/company_market.json",
                          ExtraArgs={'ACL': 'public-read'})

    return 'updated'


@app.task
def networth():
    vsm = VSMProfile.objects.all()

    for i in vsm:
        worth = 0
        h = Holding.objects.filter(user=i)

        for j in h:
            quant = j.quantity
            vlue = j.company.current_market_price
            worth += quant*vlue

        i.net_worth = worth + i.cash

        i.save()

    return 'ho gya hai bro'


@app.task
def get_all():
    with open('myfile.csv', 'w') as csv:
        for user in User.objects.all():
            d = '%s, %s, %s ,%s, %s,\n' % (
                user.id, user.username, user.email, user.last_name, user.first_name)
            csv.write(d)

    return 'done'
