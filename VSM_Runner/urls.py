from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from . import views


router = routers.DefaultRouter()
router.register(r'instruction', views.InstructionViewSet)
router.register(r'faq', views.FaqViewSet)
router.register(r'profiles', views.VSMProfileViewSet)
router.register(r'companies', views.CompanyViewSet)
router.register(r'holdings', views.HoldingViewSet)
router.register(r'leaders', views.LeaderViewSet)
router.register(r'leaders-iitb', views.IITBLViewSet)
router.register(r'tokens', views.ACTViewSet)
router.register(r'news', views.NewsViewSet)


urlpatterns = [
    path('', include(router.urls)),
    path('isact/', views.isact),
    path('me/', views.current_user),
    path('my-holdings/', views.my_holdings),
    path('trans/', views.make_transaction),
    path('test/', views.test),
    path('single_holdings/', views.single_holdings),
    path("web/", views.index, name='index'),
    path("cache_test/", views.cache_test),
    path('all_data/', views.all_data),
    path('cache_companies/', views.cache_companies),
    path('thread_companies/', views.thread_companies),
    path('t/', views.timapass),
    path('api/', views.api_hai),
    path('upload/', views.upload),
    path('register/', views.register),
    path('usr/', views.get_usr),
    path('my_holdings_token/', views.my_holdings_token),
    path('trac_token/', views.make_transaction_token),
    path('current_user_token/', views.current_user_token),
    path('single_holdings_token/', views.single_holdings_token),
    path('register_test/', views.register_test),
    path('mazak/', views.mazak),
    path('networth/', views.networth),
    path('all_vsm/', views.all_vsm),
    path('get_all/', views.get_all)

]
