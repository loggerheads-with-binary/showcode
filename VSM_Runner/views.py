from django.shortcuts import render
from rest_framework import viewsets
from rest_framework import response
from rest_framework.views import APIView
from django.contrib.auth.models import User, Group
from . import tasks
from .models import FAQ, Instruction,  VSMProfile
from .models import CompanyCMPRecord, Company, Holding, Transaction, News
from .serializers import FAQSerializer, InstructionSerializer, VSMProfileSerializer, NewsSerializer
from .serializers import CompanySerializer, CmpSerializer, HoldingSerializer, TransactionSerializer
from rest_framework.permissions import IsAuthenticated, AllowAny, IsAdminUser
from rest_framework.response import Response
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from django.http import HttpResponse
from rest_framework.decorators import api_view
from .serializers import AccessTokenSerializer
from oauth2_provider.models import AccessToken
from bsedata.bse import BSE
from decimal import Decimal
from . import models, serializers
from celery import current_app
from django.conf import settings
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from django.views.decorators.cache import cache_page, cache_control
from django.core.cache import cache
import queue
import time
from threading import Thread
import requests
from . import admin
import boto3
import datetime
import csv
from django.core.serializers import serialize
from django.utils.crypto import get_random_string


import logging

logger = logging.getLogger(__name__)


class ACTViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = AccessToken.objects.all()
    serializer_class = AccessTokenSerializer
    permission_classes = [AllowAny]


class FaqViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = FAQ.objects.all()
    serializer_class = FAQSerializer
    permission_classes = [AllowAny]


class NewsViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = News.objects.filter(is_active=True)
    serializer_class = NewsSerializer
    permission_classes = [AllowAny]


class InstructionViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Instruction.objects.all()
    serializer_class = InstructionSerializer
    permission_classes = [AllowAny]


class CompanyViewSet(viewsets.ModelViewSet):
    queryset = Company.objects.all()
    serializer_class = CompanySerializer
    permission_classes = [AllowAny]


class HoldingViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Holding.objects.all()
    serializer_class = HoldingSerializer
    permission_classes = [IsAdminUser]


@api_view(['POST'])
def single_holdings(request):
    profile = request.user.vsm_profile
    company = Company.objects.get(code=request.POST['code'])
    try:

        holdings = Holding.objects.filter(user=profile, company=company)
        holding_serializer = HoldingSerializer(
            holdings, many=True, context={'request': request})
        return Response({
            'status': 'success',
            'holding': holding_serializer.data
        })
    except:
        return Response({
            'status': 'not found',
            'holding': 'no holdings'
        })


@api_view(['GET'])
def my_holdings(request):
    profile = request.user.vsm_profile
    holdings = Holding.objects.filter(user=profile)
    holding_serializer = HoldingSerializer(
        holdings, many=True, context={'request': request})

    return Response(holding_serializer.data)


@api_view(['GET'])
def get_cmp_record(request):
    company = Company.objects.filter(code='googl')
    holdings = CmpSerializer.objects.filter()
    holding_serializer = CmpSerializer(
        holdings, many=True, context={'request': request})

    return Response(holding_serializer.data)


@api_view(['POST', 'GET'])
def make_transaction(request):

    user = request.user
    if request.method == 'GET':
        profile_serializer = VSMProfileSerializer(
            request.user.vsm_profile, context={'request': request})

        transactions = Transaction.objects.filter(user=user.vsm_profile)[:100]
        transaction_serializer = TransactionSerializer(
            transactions, many=True, context={'request': request})

        return Response(transaction_serializer.data)
    elif request.method == 'POST':
        quant = int(request.POST['quantity'])
        transac_type = request.POST['transac_type']
        code = request.POST['code']
        # company = Company.objects.get(code=code)
        user = request.user
        # price = company.current_market_price
        tasks.make_transaction.delay(quant, transac_type, code, user.email)
        # transac = Transaction.objects.create(
        #     user=user.vsm_profile, company=company, transaction_type=transac_type, quantity=quant, bid_price=price)
        # transac.save()

        # transaction_serializer = TransactionSerializer(
        #     transac, context={'request': request})

        # return Response(transaction_serializer.data)
        return Response('queue me chala gya')


@api_view(['GET', 'PATCH'])
def current_user(request):
    logger.info('current user')
    if request.method == 'GET':
        profile_serializer = VSMProfileSerializer(
            request.user.vsm_profile, context={'request': request})
        return Response(profile_serializer.data)

    elif request.method == 'PATCH':
        profile_serializer = VSMProfileSerializer(
            request.user.vsm_profile, data=request.data, partial=True, context={'request': request})
        profile_serializer.is_valid(raise_exception=True)
        profile_serializer.save()
        return Response(profile_serializer.data)


class LeaderViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = VSMProfile.objects.all().order_by('-net_worth')[:3]
    serializer_class = VSMProfileSerializer
    permission_classes = []


class IITBLViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = VSMProfile.objects.all().filter(
        is_iitb=True).order_by('-cash')[:10]
    serializer_class = VSMProfileSerializer
    permission_classes = []


class VSMProfileViewSet(viewsets.ModelViewSet):
    queryset = VSMProfile.objects.all()
    serializer_class = VSMProfileSerializer
    permission_classes = [IsAuthenticated]

    def list(self, request):
        if "sudhanshu" in self.request.user.username:
            return super().list(request)
        else:
            return Response({"status": "deny"})

    def create(self, request):
        return Response({"message": "not allowed on this path"})

    def retrieve(self, request, pk=None):
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        if self.request.user.username == serializer.data['username']:
            return Response(serializer.data)
        else:
            return Response({"access": "deny"})

    def update(self, request, pk=None):
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        if self.request.user.username == serializer.data['username']:
            return super().update(request, pk)
        elif self.request.user.username == 'sudhanshusahil':
            return super().update(request, pk)
        else:
            return Response({"access": "deny"})

    def partial_update(self, request, pk=None, *args, **kwargs):
        print("partial me aaya na")
        # kwargs['partial'] = True
        instance = self.get_object()
        serializer = self.get_serializer(
            instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        if self.request.user.username == serializer.data['username']:
            return Response(serializer.data)
        elif self.request.user.username == 'sudhanshusahil':
            return Response(serializer.data)
        else:
            return Response({"access": "deny"})

    def destroy(self, request, pk=None):
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        if self.request.user.username == serializer.data['username']:
            return super().destroy(request, pk)
        elif self.request.user.username == 'sudhanshusahil':
            return super().destroy(request, pk)
        else:
            return Response({"access": "deny"})


@api_view(['GET'])
def test(request):
    # alpha.delay()
    list = [
        '500820',
        '505200',
        '500520',
        '532281',
        '532454',
        '500180',
        '532500',
        '500300',
        '500209',
        '532174',
        '500010',
        '507685',
        '532977',
        '500087',
        '532540',
        '532978',
        '500825',
        '500247',
        '500790',
        '500124',
        '532872',
        '532783',
        '532755',
        '500114',
        '500325',
        '533278',
        '500182',
        '532187',
        '530965',
        '512070',
        '500440',
        '532898',
        '500570',
        '500696',
        '500074',
        '532538',
        '532921',
        '500034',
        '500312',
        '532555',
        '500875',
        '532215',
        '500387',
        '532488',
        '500112',
        '500228',
        '500470',
    ]
    b = BSE()
    b.updateScripCodes()
    for i in list:
        q = b.getQuote(str(i))
        name = q["companyName"]
        code = q["securityID"]
        # market_cap =q["marketCapFreeFloat"]
        current_market_price = q["currentValue"]
        change = q["change"]
        # dayhigh = q["dayHigh"]
        # daylow = q["dayLow"]
        # industry = q["industry"]
        # pChange = q["pChange"]
        # updatedon = q["updatedOn"]
        company, created = Company.objects.get_or_create(name=name, code=code)
        # company.market_cap = market_cap
        company.current_market_price = current_market_price
        company.change = change
        company.upper_limit = Decimal(current_market_price)*Decimal(1.2)
        company.lower_limit = Decimal(current_market_price)*Decimal(0.8)
        company.stocks_availible = 2000
        # company.industry = industry
        # company.pchange = pChange
        # company.updatedon = updatedon
        company.save()

    # q = b.getQuote(list[1])
    return Response('companyName')


def index(request):
    return render(request, 'index.html', context={'text': 'Hello'})


CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)


@api_view(['GET'])
def cache_test(request):

    code = 'ASIANPAINT'
    if cache.get(code):
        serializer_class = CompanySerializer(
            cache.get(code), context={'request': request})
        return Response({
            'from': 'cache',
            'data': serializer_class.data,
        })
    else:
        # queryset = Company.objects.all()
        queryset = Company.objects.get(code=code)
        # code = 'INFY'
        cache.set(code, queryset)
        serializer_class = CompanySerializer(
            queryset, context={'request': request})
        return Response({
            'from': 'db',
            'data': serializer_class.data,
        })


@api_view(['GET'])
def cache_companies(request):
    # lst = Company.objects.filter(in_trade=True).values_list('code')
    lst = [
        'ASIANPAINT',
        'EICHERMOT',
        'M&M',
        'HCLTECH',
        'BHARTIARTL',
        'HDFCBANK',
        'MARUTI',
        'GRASIM',
        'INFY',
        'ICICIBANK',
        'HDFC',
        'WIPRO',
        'BAJAJ-AUTO',
        'CIPLA',
        'TCS',
        'BAJAJFINSV',
        'BRITANNIA',
        'KOTAKBANK',
        'NESTLEIND',
        'DRREDDY',
        'TECHM',
        'TITAN',
        'RELIANCE',
        'COALINDIA',
        'HEROMOTOCO',
        'INDUSINDBK',
        'IOC',
        'UPL',
        'HINDALCO',
        'POWERGRID',
        'TATAMOTORS',
        'HINDUNILVR',
        'ULTRACEMCO',
        'ADANIPORTS',
        'BAJFINANCE',
        'ONGC',
        'NTPC',
        'ITC',
        'AXISBANK',
        'SHREECEM',
        'DIVISLAB',
        'SBIN',
        'JSWSTEEL',
        'TATASTEEL',
    ]
    from_cache = []
    ans = []

    for i in lst:
        code = i
        if cache.get(code):
            serializer_class = CompanySerializer(
                cache.get(code), context={'request': request})
            from_cache.append('cache')
            ans.append(serializer_class.data)
            # return Response({
            # 'from': 'cache',
            # 'data': serializer_class.data,
            # })

        else:
            # queryset = Company.objects.all()
            queryset = Company.objects.get(code=code)
            # code = 'INFY'
            cache.set(code, queryset)
            serializer_class = CompanySerializer(
                queryset, context={'request': request})
            from_cache.append('db')
            ans.append(serializer_class.data)
            # return Response({
            #     'from': 'db',
            #     'data': serializer_class.data,
            # })

    return Response(ans)


@api_view(['GET'])
def thread_companies(request):
    stockpicker = [
        'ASIANPAINT',
        'EICHERMOT',
        'M&M',
        'HCLTECH',
        'BHARTIARTL',
        'HDFCBANK',
        'MARUTI',
        'GRASIM',
        'INFY',
        'ICICIBANK',
        'HDFC',
        'WIPRO',
        'BAJAJ-AUTO',
        'CIPLA',
        'TCS',
        'BAJAJFINSV',
        'BRITANNIA',
        'KOTAKBANK',
        'NESTLEIND',
        'DRREDDY',
        'TECHM',
        'TITAN',
        'RELIANCE',
        'COALINDIA',
        'HEROMOTOCO',
        'INDUSINDBK',
        'IOC',
        'UPL',
        'HINDALCO',
        'POWERGRID',
        'TATAMOTORS',
        'HINDUNILVR',
        'ULTRACEMCO',
        'ADANIPORTS',
        'BAJFINANCE',
        'ONGC',
        'NTPC',
        'ITC',
        'AXISBANK',
        'SHREECEM',
        'DIVISLAB',
        'SBIN',
        'JSWSTEEL',
        'TATASTEEL',
    ]

    n_threads = len(stockpicker)

    thread_list = []
    que = queue.Queue()
    start = time.time()
    companies = []
    from_cache = []
    for i in range(n_threads):

        code = stockpicker[i]

        if cache.get(code):
            serializer_class = CompanySerializer(
                cache.get(code), context={'request': request})
            from_cache.append('cache')
        else:
            queryset = Company.objects.get(code=code)
            cache.set(code, queryset)
            serializer_class = CompanySerializer(
                queryset, context={'request': request})
            from_cache.append('db')

        thread = Thread(target=lambda q, arg1: q.put(
            {'company': serializer_class.data}), args=(que, stockpicker[i]))
        thread_list.append(thread)
        thread_list[i].start()

    for thread in thread_list:
        thread.join()

    while not que.empty():
        result = que.get()
        companies.append(result)

    end = time.time()
    time_taken = end - start
    print(time_taken)

    return Response({
        'companies': companies,
        'from': from_cache,
    })


@api_view(['GET'])
def upload(request):
    with open("/home/backend/file.json", "w") as out:
        data = serialize("json", models.Company.objects.all().order_by('name'))
        out.write(data)

    aws_access_key_id = 'AKIAXVIULOAAM7D2LP67'
    aws_secret_access_key = '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'

    # s3 = boto3.client('s3')
    s3 = boto3.client('s3',
                      aws_access_key_id=aws_access_key_id,
                      aws_secret_access_key=aws_secret_access_key)
    with open("/home/backend/file.json", "rb") as f:
        s3.upload_fileobj(f, "2k21", "vsm/file.json",
                          ExtraArgs={'ACL': 'public-read'})

    return Response("done")


@api_view(['GET'])
def all_data(request):
    b = BSE()
    q = b.getScripCodes()
    return Response(q)


# @api_view(['GET'])
# def timapass(request):
#     dekho = models.Timepass.objects.all()
#     ser = serializers.TimepassSerializer(dekho, many=True)
#     return Response(ser.data)


@api_view(['GET'])
def api_hai(request):
    URL = "https://api2.ecell.in/vsm/create_cmp/"
    myobj = {
        'code': 'somevalue',
        'name': 'name',
        'cmp': '190'
    }
    x = requests.post(URL, data=myobj)
    return Response('success')


@api_view(['POST'])
def register(request):
    reqData = request.data
    user, created = User.objects.get_or_create(username=reqData['username'])
    if created:
        try:
            user.set_password(reqData['password'])
            user.first_name = (reqData['fname'])
            user.last_name = (reqData['lname'])
            user.email = reqData['email']
            user.save()
            team, created2 = VSMProfile.objects.get_or_create(
                user=user)
            team.save()
            token = Token.objects.create(user=user)
            return Response({
                "created": True,
                "message": token.key
            })
        except:
            user.delete()
            return Response({
                "created": False,
                "message": "Something went wrong"
            })
    else:
        return Response({
            "created": False,
            "message": "User already Exist (code- " + str(user.id) + ')'
        })

# @api_view(['POST'])
# def makeUser(request):
#     regs = Registration.objects.all()
#     count = 0
#     uid = []
#     er_count = 0
#     err_ids = []
#     for reg in regs:
#         try:
#             user = User.objects.create(username=reg.email)
#             rand = get_random_string(length=6)
#             user.set_password(rand)
#             user.save()
#             reg.pw = rand
#             count+=1
#         except:
#             er_count+=1
#     return Response({
#         'a': 'Done',
#         'count' : count,
#         'er_count': er_count,
#         # 'uid': uid,
#         # 'err_ids': error
#     })


@api_view(['GET'])
def get_usr(request):
    user = request.user
    vsm = VSMProfile.objects.get(user=user)
    ser = VSMProfileSerializer(vsm)
    return Response(ser.data)


@api_view(['GET'])
def my_holdings_token(request):
    user = request.user
    profile = VSMProfile.objects.get(user=user)
    # profile = request.user.vsm_profile
    holdings = Holding.objects.filter(user=profile)
    holding_serializer = HoldingSerializer(
        holdings, many=True, context={'request': request})

    return Response(holding_serializer.data)


@api_view(['POST', 'GET'])
def make_transaction_token(request):

    user = request.user
    vsm = VSMProfile.objects.get(user=user)
    if request.method == 'GET':
        # profile_serializer = VSMProfileSerializer(
        #     request.user.vsm_profile, context={'request': request})

        profile_serializer = VSMProfileSerializer(vsm)

        transactions = Transaction.objects.filter(user=vsm)[:100]
        transaction_serializer = TransactionSerializer(
            transactions, many=True, context={'request': request})

        return Response(transaction_serializer.data)
    elif request.method == 'POST':
        quant = int(request.POST['quantity'])
        transac_type = request.POST['transac_type']
        code = request.POST['code']
        company = Company.objects.get(code=code)
        user = request.user
        vsm = VSMProfile.objects.get(user = user)
        price = company.current_market_price
        # tasks.make_transaction.delay(quant, transac_type, code, user.email)
        transac = Transaction.objects.create(
            user=vsm, company=company, transaction_type=transac_type, quantity=quant, bid_price=price)
        transac.save()

        transaction_serializer = TransactionSerializer(
            transac, context={'request': request})

        return Response(transaction_serializer.data)
        return Response('queue me chala gya')


@api_view(['GET', 'PATCH'])
def current_user_token(request):
    logger.info('current user')
    if request.method == 'GET':
        profile_serializer = VSMProfileSerializer(
            request.user.vsm_profile, context={'request': request})
        return Response(profile_serializer.data)

    elif request.method == 'PATCH':

        user = request.user
        vsm = VSMProfile.objects.get(user=user)
        profile_serializer = VSMProfileSerializer(
            vsm, data=request.data, partial=True, context={'request': request})
        profile_serializer.is_valid(raise_exception=True)
        profile_serializer.save()
        return Response(profile_serializer.data)


@api_view(['POST'])
def single_holdings_token(request):
    user = request.user
    profile = VSMProfile.objects.get(user=user)
    # return Response('success')
    company = Company.objects.get(code=request.POST['code'])
    try:

        holdings = Holding.objects.filter(user=profile, company=company)
        holding_serializer = HoldingSerializer(
            holdings, many=True)
        return Response({
            'status': 'success',
            'holding': holding_serializer.data
        })
    except:
        return Response({
            'status': 'not found',
            'holding': 'no holdings'
        })


# @api_view(['POST'])
# def makeUser(request):
#     regs = Registration.objects.all()
#     count = 0
#     uid = []
#     er_count = 0
#     err_ids = []
#     for reg in regs:
#         try:
#             uname = reg.email.split("@")[0]
#             user = User.objects.create(username=uname)
#             rand = get_random_string(length=6)
#             user.set_password(rand)
#             user.save()
#             reg.pw = rand
#             reg.uname = uname
#             reg.save()
#             count+=1
#         except:
#             er_count+=1
#     return Response({
#         'a': 'Done',
#         'count' : count,
#         'er_count': er_count,
#         # 'uid': uid,
#         # 'err_ids': error
#     })
@api_view(['GET'])
def register_test(request):
    uname = User.objects.make_random_password()
    password = User.objects.make_random_password()
    new_user = User.objects.create(username=uname)
    new_user.set_password(password)
    new_user.save()
    return Response('done dana done')


@api_view(['GET'])
def mazak(request):
    return Response('done1')


@api_view(['GET'])
def networth(request):
    tasks.networth.delay()
    return Response('bas ho gya yaar')


@api_view(['GET'])
def all_vsm(request):
    stockpicker = VSMProfile.objects.all()
    n_threads = stockpicker.count()

    thread_list = []
    que = queue.Queue()
    start = time.time()
    companies = []
    j = 0
    for i in range(n_threads):
        queryset = stockpicker[i]
        if queryset.cash != 1250000.00:
            serializer_class = VSMProfileSerializer(
                queryset, context={'request': request})

            thread = Thread(target=lambda q, arg1: q.put(
                {'chutiya': serializer_class.data}), args=(que, stockpicker[i]))
            thread_list.append(thread)
            thread_list[j].start()
            j += 1
        else:
            pass

    for thread in thread_list:
        thread.join()

    while not que.empty():
        result = que.get()
        companies.append(result)

    end = time.time()
    time_taken = end - start
    print(time_taken)
    # abc = stockpicker[1]
    # ser = VSMProfileSerializer(abc,context={'request': request})

    return Response(companies)


# def download_csv(request, queryset):
#     if not request.user.is_staff:
#         raise PermissionDenied

#     model = queryset.model
#     model_fields = model._meta.fields + model._meta.many_to_many
#     field_names = [field.name for field in model_fields]

#     response = HttpResponse(content_type='text/csv')
#     response['Content-Disposition'] = 'attachment; filename="export.csv"'

#     # the csv writer
#     writer = csv.writer(response, delimiter=";")
#     # Write a first row with header information
#     writer.writerow(field_names)
#     # Write data rows
#     for row in queryset:
#         values = []
#         for field in field_names:
#             value = getattr(row, field)
#             if callable(value):
#                 try:
#                     value = value() or ''
#                 except:
#                     value = 'Error retrieving value'
#             if value is None:
#                 value = ''
#             values.append(value)
#         writer.writerow(values)
#     return response

@api_view(['GET'])
def get_all(request):
    # data = download_csv(request, User.objects.all())
    # response = HttpResponse(data, content_type='text/csv')
    # response = admin.UserResource().export()
    # obj = User.objects.all()[:50]
    # ser = serializers.UserSerializer(obj, many=True)
    tasks.get_all.delay()
    return Response('data')
    # stockpicker = VSMProfile.objects.all()
    # n_threads = stockpicker.count()

    # thread_list = []
    # que = queue.Queue()
    # start = time.time()
    # companies = []
    # j=0
    # for i in range(n_threads):
    #     queryset = stockpicker[i]
    #     if queryset.cash != 1250000.00:
    #         serializer_class = VSMProfileSerializer(queryset)

    #         thread = Thread(target = lambda q, arg1: q.put({'chutiya': serializer_class.data}), args = (que, stockpicker[i]))
    #         thread_list.append(thread)
    #         thread_list[j].start()
    #         j+=1
    #     else:
    #         pass

    # for thread in thread_list:
    #     thread.join()

    # while not que.empty():
    #     result = que.get()
    #     companies.append(result)

    # end = time.time()
    # time_taken =  end - start
    # print(time_taken)
    # # abc = stockpicker[1]
    # # ser = VSMProfileSerializer(abc,context={'request': request})

    # return 'done'

@api_view(['GET'])
def isact(request):
    name= 'vsm'
    mod = models.Timepass.objects.get(name=name)
    if mod.active:
        return Response('true')
    else:
        return Response('false')