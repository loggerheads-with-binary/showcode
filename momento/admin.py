from django.contrib import admin
from . import models
# Register your models here.
from import_export.admin import ImportExportModelAdmin

admin.site.register(models.Template)
# admin.site.register(models.Speakers)
admin.site.register(models.Linkedin)
admin.site.register(models.Converter)


class SpeakersAdmin(ImportExportModelAdmin):
    list_display = ('name', 'email' , 'year')
    search_fields = ['name']


admin.site.register(models.Speakers, SpeakersAdmin)

