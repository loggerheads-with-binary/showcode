from django.db import models
from django.contrib.auth.models import User
import os
import uuid

def upload_path(instance, filename):
    ext = filename.split('.')[-1]
    filename = "%s_%s-%s.%s" % (instance.momento_name,
                                instance.year, instance.id, ext)
    return '/'.join(['certi', filename])


class Template(models.Model):
    file = models.FileField(blank=True, null=True, upload_to=upload_path)
    year = models.CharField(max_length=9, null=True,
                            blank=True, default="2021-2022")
    momento_name = models.CharField(
        max_length=50, null=True, blank=True)
    font = models.IntegerField(null=True, blank=True)
    font_file = models.FileField(blank=True, null=True)

    x = models. IntegerField(null=True, blank=True)
    y = models. IntegerField(null=True, blank=True)

    x_image = models.IntegerField(null=True, blank=True)
    y_image = models.IntegerField(null=True, blank=True)

    image_size = models.IntegerField(null=True, blank=True, default=800)

    border_thickness = models.IntegerField(null=True, blank=True, default=20)

    border_radius = models.IntegerField(null=True, blank=True, default=100)

    caption = models.TextField(null=True, blank=True)

    def __str__(self):
        return f"{self.momento_name} -- {self.year}"

    class Meta:
        unique_together = ('momento_name', 'year')


class Speakers(models.Model):
    email = models.EmailField(max_length=251, null=False, blank=True)
    name = models.CharField(max_length=60, null=True, blank=True)
    year = models.CharField(max_length=9, null=True,
                            blank=True, default="2021-2022")
    template = models.CharField(null=True, blank=True, max_length=255)

    class Meta:
        unique_together = ('email', 'template', 'year')

    def __str__(self):
        return f"{self.name} -- {self.email} -- {self.template}"

class Linkedin(models.Model):
    user = models.TextField(null=True, blank=True)
    token = models.TextField(null=True, blank=True)
    expire_on = models.TextField(null=True, blank=True)

class Converter(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user = models.ForeignKey(Speakers, on_delete=models.CASCADE)
    token = models.TextField(null=True, blank=True)
    image = models.TextField(null=True, blank=True)