from rest_framework import serializers
from . import models
from django.contrib.auth.models import User


class TemplateSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Template
        fields = '__all__'

class SpeakersSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Speakers
        fields = '__all__'

class LinkedinSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Linkedin
        fields = '__all__'


class ConverterSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Converter
        fields = '__all__'
