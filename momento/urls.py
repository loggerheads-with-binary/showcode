from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from . import views

router = routers.DefaultRouter()


urlpatterns = [
    path('call_url/', views.call_url),
    path('detailc/', views.candidate),
    path('template/', views.template),
    path('token/', views.token),
    path('conv/', views.conv),
    # path('test/', views.test),
    path('share/', views.share),
    # path('check_phone/', views.check_phone),
    path("bt/", views.bt)
]
