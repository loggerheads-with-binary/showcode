from django.db.models import fields
from django.http.response import HttpResponse
from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework import viewsets
from rest_framework.views import APIView
from django.core.mail import send_mail
from . import models, serializers
import requests    
import urllib.request
import boto3
import random
import string 
import os
import base64
from pdf2image import convert_from_path

# Create your views here.



@api_view(['POST'])
def candidate(request):
    email = request.data['email']
    year = request.data['year']
    momento = models.Speakers.objects.filter(email=email, year=year)
    momento_serializer = serializers.SpeakersSerializer(momento, many=True)
    return Response(momento_serializer.data)

@api_view(['POST'])
def template(request):
    name = request.data['name']
    year = request.data['year']
    momento = models.Template.objects.get(momento_name=name, year=year)
    momento_serializer = serializers.TemplateSerializer(momento)
    return Response(momento_serializer.data)


@api_view(['GET','POST'])
def call_url(request):
    url = request.POST['url']
    response = requests.get(url)
    res = response.json()
    access_token = res['access_token']

    token = 'Bearer ' + access_token
    my_headers = {'Authorization' : token}
    response = requests.get('https://api.linkedin.com/v2/me?projection=(id,firstName,lastName,profilePicture(displayImage~:playableStreams))', headers=my_headers)
    res = response.json()
    res = res['profilePicture']['displayImage~']['elements'][3]['identifiers'][0]['identifier']


    a = ''.join(random.choices(string.ascii_uppercase + string.digits, k = 5)) 

    file = str(a)+'.jpg'


    f = open(file,'wb')
    f.write(urllib.request.urlopen(res).read())
    f.close()


    AWS_ACCESS_KEY_ID = 'AKIAXVIULOAALQE5Y77Q'
    WS_SECRET_ACCESS_KEY = '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'

    s3 = boto3.client('s3',
        aws_access_key_id=AWS_ACCESS_KEY_ID,
        aws_secret_access_key= WS_SECRET_ACCESS_KEY)

    to = "momento/"+file

    with open(file, "rb") as f:
        t= s3.upload_fileobj(f, "2k21", to, ExtraArgs={'ACL':'public-read'})


    url = 'https://2k21.s3.ap-south-1.amazonaws.com/momento/'+file
    os.remove(file)
    return Response({
        'url':url,
        'access_token':access_token
        })
    
@api_view(['GET','POST'])
def token(request):
    url = request.POST['url']
    response = requests.get(url)
    res = response.json()
    access_token = res['access_token']

    token = 'Bearer ' + access_token
    my_headers = {'Authorization' : token}

    url = 'https://api.linkedin.com/v2/me'

    r = requests.get(url, headers=my_headers)
    r = r.json()

    return Response({
        'abc':res, 
        'def':r
        })


@api_view(['GET','POST'])
def conv(request):
    url = request.POST['data']
    user = models.Speakers.objects.get(template=request.POST['template'], email = request.POST['email'], year = request.POST['year'])
    conv = models.Converter.objects.create(token = url, user=user)
    pdf = conv.token
    a = ''.join(random.choices(string.ascii_uppercase + string.digits, k = 5)) 
    base = '/home/backend/static/image/'
    file_pdf =  str(a)+'.pdf'
    file_jpg = str(a)+'.jpg'


    pdf = base64.b64decode(pdf)
    with open(base +file_pdf, 'wb') as fout:
        fout.write(pdf)

    images = convert_from_path(base +file_pdf)
    for page in images:
        page.save(base +file_jpg, 'JPEG')
    
    # os.remove(base +file_pdf)

    AWS_ACCESS_KEY_ID = 'AKIAXVIULOAALQE5Y77Q'
    WS_SECRET_ACCESS_KEY = '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'

    s3 = boto3.client('s3',
        aws_access_key_id=AWS_ACCESS_KEY_ID,
        aws_secret_access_key= WS_SECRET_ACCESS_KEY)

    to = "momento/"+file_jpg

    with open(base+file_jpg, "rb") as f:
        t= s3.upload_fileobj(f, "2k21", to, ExtraArgs={'ACL':'public-read'})
        url = 'https://2k21.s3.ap-south-1.amazonaws.com/momento/'+file_jpg
        os.remove(base+file_jpg)
        os.remove(base +file_pdf)
        # return Response(url)

    conv.image = url
    conv.save()

    return Response(a)



@api_view(['GET','POST'])
def share(request):
    token = request.POST['token']
    # response = requests.get(url)
    # res = response.json()
    # access_token = res['access_token']
    access_token = token

    token = 'Bearer ' + access_token
    my_headers = {'Authorization' : token}

    url = 'https://api.linkedin.com/v2/me'

    r = requests.get(url, headers=my_headers)
    r = r.json()
    id = r["id"]
    id = "urn:li:person:"+id
    
    temp = request.POST['id']
    template = models.Template.objects.get(momento_name = temp)
    caption = template.caption

    code = request.POST['code']
    shared =  "https://ecell.in/momento/share/?q="+code


    data = {
        "author": id,
        "lifecycleState": "PUBLISHED",
        "specificContent": {
            "com.linkedin.ugc.ShareContent": {
                "shareCommentary": {
                    "text": caption
                },
                "shareMediaCategory": "ARTICLE",
                "media": [
                    {
                        "status": "READY",
                        "description": {
                            "text": "Official LinkedIn Blog - Your source for insights and information about LinkedIn."
                        },
                        "originalUrl": shared,
                        "title": {
                            "text": "Momento"
                        }
                    }
                ]
            }
        },
        "visibility": {
            "com.linkedin.ugc.MemberNetworkVisibility": "PUBLIC"
        }
    }

    url = 'https://api.linkedin.com/v2/ugcPosts/'
    res = requests.post(url, json=data, headers=my_headers)
    res  =res.json()


    return Response(res)


#     my_obj = {
#    "registerUploadRequest":{
#       "owner":"urn:li:organization:24141830",
#       "recipes":[
#          "urn:li:digitalmediaRecipe:feedshare-image"
#       ],
#       "serviceRelationships":[
#          {
#             "identifier":"urn:li:userGeneratedContent",
#             "relationshipType":"OWNER"
#          }
#       ],
#       "supportedUploadMechanism":[
#          "SYNCHRONOUS_UPLOAD"
#       ]
#    }
# }

#     response = requests.post('https://api.linkedin.com/v2/assets?action=registerUpload',json = my_obj,headers=my_headers)
#     res = response.json()
#     url = res['value']['uploadMechanism']['com.linkedin.digitalmedia.uploading.MediaUploadHttpRequest']['uploadUrl']
#     file = '/home/backend/static/image/CJOSU.jpg'
#     files = {'upload-file': open(file,'rb')}
#     r = requests.post(url, files=files, headers=my_headers)
#     res = r.json()

@api_view(['GET', 'POST'])
def bt(request):
    user = models.Speakers.objects.get(template=request.POST['template'], email = request.POST['email'], year = request.POST['year'])
    ser = serializers.SpeakersSerializer(user)
    return Response(ser.data)