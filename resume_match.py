import fitz
from . import models
import json
import subprocess 
import os 


import pandas as pd 
from sklearn.multiclass import OneVsRestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.feature_extraction import text
TfidfVectorizer = text.TfidfVectorizer 
from sklearn.model_selection import train_test_split


PARAMETERS = {'basic_score' : 0.2 , }
RESUME_DIR = '/tmp/fincof/resumes/temp' ##Temporary storage
COUNTER = 0 
DATA_DIR = '/home/fincof/rmatch/data'
MODEL0, MODEL1 = None, None  
MODEL_BUILT = False 

def resume_download(link):

    aws_access_key_id = 'AKIAXVIULOAAM7D2LP67'
    aws_secret_access_key = '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'

    # s3 = boto3.client('s3')
    s3 = boto3.client('s3',
                      aws_access_key_id=aws_access_key_id,
                      aws_secret_access_key=aws_secret_access_key)


    fpath = os.path.join(RESUME_DIR , f'Resume-{COUNTER}.pdf') 
    s3.download_file('fincof' , link , fpath)

    return fpath 

def get_tdata():

    ##Data consisted of 'resume-text' , 'jd' , 'title' , 'match' 
    fpath = os.path.join(DATA_DIR , 'training.csv')
    data = pd.read_csv(fpath).drop_duplicates()
    data['resume-text'] = data['resume-text'].astype(str)
    data['jd'] = data['jd'].astype(str)
    data['title'] = data['title'].astype(str)
    data['match'] = data['match'].astype(float)
    return data 

def build_model():

    global MODEL0, MODEL1,  MODEL_BUILT, vv, v1, v2

    data = get_tdata()

    vals = data['text']
    cats = data['title']
    matches = data['match']
    jds = data['jd']

    vv = TfidfVectorizer(
    sublinear_tf=True,
    stop_words='english',
    max_features=150)

    
    vv.fit(vals)
    features = vv.transform(vals)
    X_train,X_test,y_train,y_test = train_test_split(features , cats , test_size = 0.1)

    MODEL0 = OneVsRestClassifier(KNeighborsClassifier()) 
    MODEL0.fit(X_train, y_train) 
    # prediction = clf.predict(X_test)
    # print(clf.score(X_test, y_test)) : 93% naacho 

    v1 = TfidfVectorizer(
    sublinear_tf=True,
    stop_words='english',
    max_features=250)

    v2 = TfidfVectorizer(
    sublinear_tf = True , 
    stop_words = 'english' , 
    max_features = 350
    )

    vv.fit(vals)
    v2.fit(jds)
    features = vv.transform(vals)
    jds = vv.transform(jds)

    X_train,X_test,y_train,y_test = train_test_split(features , jds ,  test_size = 0.1)


    MODEL1 = OneVsRestClassifier(KNeighborsClassifier()) 
    MODEL1.fit(X_train, y_train) 
    #Thoda kam accurate. Rakhlo firbhi. Kya hi jaa raha  

    MODEL_BUILT = True 

    return 'naacho'

def _match(resume:str , offering : models.Offering):

    global MODEL0, MODEL1,  MODEL_BUILT, vv, v1, v2

    if MODEL_BUILT is False:
        build_model()         

    jd =  offering.job
    title = offering.title 

    r1 = vv.transform(resume)
    r2 = v1.transform(resume)
    r3 = v2.transform(jd)

    score_1 = MODEL0.predict((r1 , title))      ##Returns an accuracy value in fraction 
    score_2 = MODEL1.predict((r2 , r3))         ##Returns an accuracy 

    return score_1*((1+score_2)/(2+score_2))

#Returns False if basics fail, otherwise returns int 
def match(application : models.Application , offering : models.Offering):

    kwords = set(map(lambda x : x.strip().lower() , list(json.loads(offering.keywords))))  #Get a list 
    resume = resume_download(application.resume)
    
    with fitz.open(resume) as f:
        r_text = '\n'.join(list(map(lambda x : x.get_text() , f )))

    basic_score = sum(map(lambda x : 1 if (x in r_text) else 0 , kwords))
    
    if len(kwords) > 10:
        basic_score = 0.1*basic_score 

    else:
        basic_score = basic_score/len(kwords)

    if basic_score < PARAMETERS['basic_score']:
        return False 

    ##AI Matching starts 
    return _match(r_text , offering)



