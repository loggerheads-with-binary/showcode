from django.contrib import admin
from . import models
from import_export.admin import ImportExportModelAdmin
# Register your models here.

# admin.site.register(models.Registration)

class ApplicationAdmin(ImportExportModelAdmin):
    list_display = ('name', 'email', 'whatsapp')
    search_fields = ['name', 'email', 'whatsapp']


admin.site.register(models.Registration, ApplicationAdmin)

admin.site.register(models.Media)
admin.site.register(models.Sponsors)
admin.site.register(models.Resources)
admin.site.register(models.Active)