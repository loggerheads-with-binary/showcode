from django.db import models

# Create your models here.

class Registration(models.Model):
    name =models.CharField(null=True, max_length=251)
    email = models.EmailField(null=True)
    whatsapp = models.TextField()
    refferal = models.TextField(null=True, blank=True)

    def __str__(self):
        return f"{self.name}"


class Sponsors(models.Model):
    name =models.TextField(null=True,blank=True)
    email = models.FileField(null=True, blank=True)
    url = models.URLField(null=True, blank=True)

    def __str__(self):
        return f"{self.name}"

class Media(models.Model):
    name =models.TextField(null=True,blank=True)
    email = models.FileField(null=True, blank=True)
    url = models.URLField(null=True, blank=True)

    def __str__(self):
        return f"{self.name}"


class Resources(models.Model):
    name =models.TextField(null=True,blank=True)
    image = models.FileField(null=True, blank=True)
    url = models.URLField(null=True, blank=True)


    def __str__(self):
        return f"{self.name}"


class Active(models.Model):
    name =models.TextField(null=True,blank=True)
    image = models.FileField(null=True, blank=True)
    url = models.URLField(null=True, blank=True)
    value= models.BooleanField(default=False)


    def __str__(self):
        return f"{self.name}"