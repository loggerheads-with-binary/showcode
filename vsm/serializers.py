from rest_framework import serializers
from . import models
from django.contrib.auth.models import User


class RegistrationSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Registration
        fields = '__all__'

class MediaSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Media
        fields = '__all__'


class SponsorSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Sponsors
        fields = '__all__'

class ResourcesSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Resources
        fields = '__all__'



class ActiveSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Active
        fields = '__all__'
