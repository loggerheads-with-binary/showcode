from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from . import views

router = routers.DefaultRouter()
# router.register(r'student', StudentViewSet)
# router.register(r'managers', ManagerViewSet)
# router.register(r'portfolios', PortfolioViewSet)

urlpatterns = [
    path('reg/', views.submit),
    path('partners/', views.partners),
    path('resource/',views.resource),
    path('leader/', views.leader),
]
