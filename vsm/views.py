from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework import viewsets
from rest_framework.views import APIView
from django.core.mail import send_mail
from . import models, serializers
# Create your views here.


@api_view(['POST'])
def submit(request):
    reqdata = request.data
    sub, created = models.Registration.objects.get_or_create(email = reqdata['email'])
    sub.name = reqdata['name']
    sub.whatsapp= reqdata['whatsapp']
    sub.refferal = reqdata['refferal']
    sub.save()
    ser = serializers.RegistrationSerializer(sub)
    return Response(ser.data)

@api_view(['GET'])
def partners(request):
    spon = models.Sponsors.objects.all()
    media = models.Media.objects.all()
    ser_media = serializers.MediaSerializer(media, many=True)
    ser_spon = serializers.SponsorSerializer(spon, many = True)

    return Response({
        'sponsors': ser_spon.data,
        'media': ser_media.data,
    })

@api_view(['GET'])
def resource(request):
    spon = models.Resources.objects.all()
    ser_spon = serializers.ResourcesSerializer(spon, many = True)

    return Response(ser_spon.data)

@api_view(['GET'])
def leader(request):
    spon = models.Active.objects.get(name='leader')
    ser_spon = serializers.ActiveSerializer(spon)

    return Response(ser_spon.data)